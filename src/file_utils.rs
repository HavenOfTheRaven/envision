use crate::{cmd_runner::CmdRunner, runner::Runner};
use nix::{
    errno::Errno,
    sys::statvfs::{statvfs, FsFlags},
};
use std::{
    fs::{self, copy, create_dir_all, remove_dir_all, File, OpenOptions},
    io::{BufReader, BufWriter},
    path::Path,
};

pub fn get_writer(path_s: &str) -> anyhow::Result<BufWriter<std::fs::File>> {
    let path = Path::new(path_s);
    if let Some(parent) = path.parent() {
        if !parent.is_dir() {
            create_dir_all(parent)?;
        }
    };
    let file = OpenOptions::new()
        .write(true)
        .create(true)
        .truncate(true)
        .open(path)?;
    Ok(BufWriter::new(file))
}

#[deprecated]
pub fn get_writer_legacy(path_s: &str) -> BufWriter<std::fs::File> {
    let path = Path::new(path_s);
    if let Some(parent) = path.parent() {
        if !parent.is_dir() {
            create_dir_all(parent).expect("Could not create dir")
        }
    };
    let file = OpenOptions::new()
        .write(true)
        .create(true)
        .truncate(true)
        .open(path)
        .expect("Could not open file");
    BufWriter::new(file)
}

pub fn get_reader(path_s: &str) -> Option<BufReader<File>> {
    let path = Path::new(&path_s);
    if !(path.is_file() || path.is_symlink()) {
        return None;
    }
    match File::open(path) {
        Err(e) => {
            println!("Error opening {}: {}", path_s, e);
            None
        }
        Ok(fd) => Some(BufReader::new(fd)),
    }
}

pub fn deserialize_file<T: serde::de::DeserializeOwned>(path_s: &String) -> Option<T> {
    match get_reader(path_s) {
        None => None,
        Some(reader) => match serde_json::from_reader(reader) {
            Err(e) => {
                println!("Failed to deserialize {}: {}", path_s, e);
                None
            }
            Ok(res) => Some(res),
        },
    }
}

pub fn set_file_readonly(path_s: &str, readonly: bool) -> Result<(), std::io::Error> {
    let path = Path::new(&path_s);
    if !path.is_file() {
        println!("WARN: trying to set readonly on a file that does not exist");
        return Ok(());
    }
    let mut perms = fs::metadata(path)
        .expect("Could not get metadata for file")
        .permissions();
    perms.set_readonly(readonly);
    fs::set_permissions(path, perms)
}

pub fn setcap_cap_sys_nice_eip(file: String) {
    let mut runner = CmdRunner::new(
        None,
        "pkexec".into(),
        vec!["setcap".into(), "CAP_SYS_NICE=eip".into(), file],
    );
    runner.start();
    runner.join();
}

pub fn rm_rf(path_s: &String) {
    if remove_dir_all(path_s).is_err() {
        println!("Failed to remove path {}", path_s);
    }
}

pub fn copy_file(source_s: &str, dest_s: &str) {
    let source = Path::new(source_s);
    let dest = Path::new(dest_s);
    if let Some(parent) = dest.parent() {
        if !parent.is_dir() {
            create_dir_all(parent)
                .unwrap_or_else(|_| panic!("Failed to create dir {}", parent.to_str().unwrap()));
        }
    }
    set_file_readonly(dest_s, false)
        .unwrap_or_else(|_| panic!("Failed to set file {} as rw", dest_s));
    copy(source, dest).unwrap_or_else(|_| panic!("Failed to copy {} to {}", source_s, dest_s));
}

pub fn mount_has_nosuid(path_s: &str) -> Result<bool, Errno> {
    let path = Path::new(path_s);
    match statvfs(path) {
        Ok(fstats) => Ok(fstats.flags().contains(FsFlags::ST_NOSUID)),
        Err(e) => Err(e),
    }
}

#[cfg(test)]
mod tests {
    use crate::file_utils::mount_has_nosuid;

    #[test]
    fn can_get_nosuid() {
        mount_has_nosuid("/tmp").expect("Error running statvfs");
    }
}
